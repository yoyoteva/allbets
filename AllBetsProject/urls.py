"""
Definition of urls for AllBetsProject.
"""

from datetime import datetime
from django.urls import re_path, path, include
from django.contrib import admin
from allauth.account.views import LoginView, LogoutView
from app import forms, views


urlpatterns = [
    path('', views.home, name='home'),
    path('contact/', views.contact, name='contact'),
    path('about/', views.about, name='about'),
    #path('login/',
    #     LoginView.as_view
    #     (
    #         template_name='app/login.html',
    #         authentication_form=forms.BootstrapAuthenticationForm,
    #         extra_context=
    #         {
    #             'title': 'Log in',
    #             'year' : datetime.now().year,
    #         }
    #     ),
    #     name='login'),
    #path('logout/', LogoutView.as_view(next_page='/'), name='logout'),
    path('admin/', admin.site.urls, name='admin'),
    #ACCOUNTS
    path('accounts/profile/', views.profile, name='profile'),
    path('accounts/my_bank/', views.MyBankView.as_view(), name='my_bank'),
    path('accounts/login/', views.MyLoginView.as_view(), name='login'),
    re_path(r'^accounts/', include('allauth.urls')),
    #BETS
    path('bets/all/', views.BetListView.as_view(), name='all_bets'),
    path('bets/<int:pk>/details/', views.BetDetailView.as_view(), name='bet_detail'),
    path('bets/new/', views.BetCreationView.as_view(), name='bet_creation'),
    path('bets/<int:pk>/create_betting/', views.CreateBet.as_view(), name='betting_creation'),
    path('bets/my_bets/', views.MyBetsView.as_view(), name='my_bets'),
    path('bets/<int:pk>/admin/', views.BetAdminView.as_view(), name='admin_bet'),
]
