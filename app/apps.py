from django.apps import AppConfig

class AllbetsConfig(AppConfig):
    name = 'app'

    def ready(self):
        import app.signals  # noqa
