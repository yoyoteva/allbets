from django.contrib import admin
from app.models import *

#class BankAcccountAdmin(admin.ModelAdmin):
    #"""Definition of the BankAccount editor."""
    

admin.site.register(BankAccount)
admin.site.register(BankOperation)
admin.site.register(Bet)
admin.site.register(BetOption)
admin.site.register(Betting)