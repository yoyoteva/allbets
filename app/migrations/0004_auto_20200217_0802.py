# Generated by Django 2.2.10 on 2020-02-17 07:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0003_auto_20200217_0650'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Option',
            new_name='BetOption',
        ),
        migrations.AddField(
            model_name='betting',
            name='option',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='app.BetOption'),
            preserve_default=False,
        ),
    ]
