"""
Definition of models.
"""

from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import User
from django.dispatch import receiver
from allauth.account.signals import email_confirmed, user_signed_up
from django.urls import reverse
from decimal import *


class BankAccount(models.Model):
    balance = models.DecimalField(max_digits=20, decimal_places=2)
    authorized_overdraft = models.DecimalField(max_digits=20, decimal_places=2)
    date_open = models.DateTimeField(auto_now_add=True)
    closed = models.BooleanField()
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    def __str__(self):
        return self.user.username + '\'s account'


class BankOperation(models.Model):
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=264)
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    account = models.ForeignKey(BankAccount, on_delete=models.SET_NULL, null=True)


class Bet(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    expiration_date = models.DateTimeField()
    creator = models.ForeignKey(User, on_delete=models.SET_DEFAULT, default=User.objects.get(username="AllBets").pk)
    extendable = models.BooleanField()
    title = models.CharField(max_length=264)
    summary = models.TextField()

    def get_entries_count(self): 
        return Betting.objects.filter(option__bet=self).count()

    def get_total_invested(self):
        sum = Betting.objects.filter(option__bet=self).aggregate(Sum('amount'))['amount__sum']
        return float(sum) if sum is not None else 0

    def __str__(self):
        return self.title + " by " + self.creator.username

    def get_absolute_url(self):
        return reverse('bet_detail', args=[self.id])


class BetOption(models.Model):

    creation_date = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(User, on_delete=models.SET_DEFAULT, default=User.objects.get(username="AllBets").pk)
    title = models.CharField(max_length=264)
    rating = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    bet = models.ForeignKey(Bet, on_delete=models.CASCADE)

    def get_entries_count(self): 
        return Betting.objects.filter(option=self).count()

    def get_total_invested(self):
        sum = Betting.objects.filter(option=self).aggregate(Sum('amount'))['amount__sum']
        return float(sum) if sum is not None else 0

    def get_current_rating(self):
        self_total = self.get_total_invested()
        if self_total > 0:
            rate = self.bet.get_total_invested()*0.85/self.get_total_invested()
            return round(rate, 2)
        else: return 0

    def __str__(self):
        return self.bet.title + " : " + self.title


class Betting(models.Model):

    date = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    option = models.ForeignKey(BetOption, on_delete=models.CASCADE)

    def get_current_payback(self):
        payback = self.amount*Decimal(self.option.get_current_rating())
        return round(payback, 2)

    def __str__(self):
        return self.creator.username + "'s betting " + str(self.amount) + " on option " + str(self.option)