"""
Definition of views.
"""

from datetime import datetime
from django.shortcuts import render, get_object_or_404
from django.http import HttpRequest, HttpResponseForbidden, HttpResponseRedirect
from allauth.account.views import SignupView, LoginView
from app.models import *
from django.db.models import Q
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.views.generic.detail import SingleObjectMixin
from app.forms import *
from decimal import *


def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )

def profile(request):
    """Renders the profile page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'account/profile.html',
        {
            'title':'Profile',
            'message':'Authentified profile details',
            'year':datetime.now().year,
        }
    )

#def account_login(request):
#    """Renders the login page."""
#    assert isinstance(request, HttpRequest)
#    return render(
#        request,
#        'account/login.html',
#        {
#            'title':'Login',
#            'year':datetime.now().year,
#        }
#    )

class MyLoginView(LoginView):

    template_name = 'account/login.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['title'] = 'Login'
        context['year'] = datetime.now().year
        return context


class MyBankView(ListView):

    template_name = 'account/my_bank.html'
    context_object_name = 'last_operations'

    def get_queryset(self):
        bank_account = user=self.request.user.bankaccount
        # Retrieve the last 15 operations for this account
        return BankOperation.objects.filter(account=bank_account).order_by('-date')[:5]

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['title'] = 'My Bank'
        context['year'] = datetime.now().year
        return context


class BetListView(ListView):

    model = Bet
    paginate_by = 50
    template_name = 'bets/bet_list.html'
    context_object_name = 'bet_list'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['title'] = 'All Bets'
        context['year'] = datetime.now().year

        return context


class BetDetailView(DetailView):

    model = Bet
    template_name = 'bets/bet_detail.html'
    context_object_name = 'bet'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['title'] = 'Bet Detail'
        context['year'] = datetime.now().year

        return context


class BetCreationView(CreateView):

    model = Bet
    template_name = 'bets/bet_creation.html'
    form_class = BetForm

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['title'] = 'Create a new bet'
        context['year'] = datetime.now().year
        if self.request.POST:
            context['betoptions'] = BetOptionFormset(self.request.POST)
        else:
            context['betoptions'] = BetOptionFormset()
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        betoptions = context["betoptions"]
        self.object = form.save()
        self.object.creator = self.request.user
        if betoptions.is_valid():
            betoptions.instance = self.object
            betoptions.instance.creator = self.request.user
            betoptions.save()
        else:
            print(betoptions)
        return super().form_valid(form)


class BetAdminView(UpdateView):

    model = Bet
    template_name = 'bets/bet_update.html'
    form_class = BetForm

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = 'Bet administration'
        context['year'] = datetime.now().year
        if self.request.POST:
            context['betoptions'] = BetOptionFormset(self.request.POST)
        else:
            context['betoptions'] = BetOptionFormset(instance=self.object)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        betoptions = context["betoptions"]
        self.object = form.save()
        self.object.creator = self.request.user
        if betoptions.is_valid():
            betoptions.instance = self.object
            betoptions.instance.creator = self.request.user
            betoptions.save()
        else:
            print(betoptions)
        return super().form_valid(form)


class MyBetsView(ListView):

    template_name = 'bets/my_bets.html'
    context_object_name = 'active_bets'

    def get_queryset(self):
        # Retrieve the bets active bets that this user created ordered by desc creation date
        return Bet.objects.filter(creator=self.request.user).order_by('-creation_date')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['title'] = 'My Bets'
        context['year'] = datetime.now().year
        # Retrieve the bets active bettings that this user created ordered by desc creation date
        context['active_bettings'] = Betting.objects.filter(creator=self.request.user).order_by('-date')
        return context




class CreateBet(SingleObjectMixin, View):
    """Create a betting for the connected user"""
    model = Bet

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()

        # Look up the author we're interested in.
        self.object = self.get_object()
        # Actually record interest somehow here!
        amount = Decimal(request.POST.get('amount'))
        option = BetOption.objects.get(pk=request.POST.get('betoption'))

        if request.user.bankaccount.balance >= amount:
             b = Betting(amount=amount, option=option, creator=request.user)
             b.save()
        else:
            return HttpResponseForbidden()
        return HttpResponseRedirect(reverse('bet_detail', kwargs={'pk': self.object.pk}))


