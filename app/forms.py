"""
Definition of forms.
"""

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from app.models import *
from django.forms.models import inlineformset_factory
from crispy_forms.layout import LayoutObject, TEMPLATE_PACK, Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
from django.shortcuts import render
from django.template.loader import render_to_string
from crispy_forms.helper import FormHelper

class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder':'Password'}))

class BetForm(forms.ModelForm):

    class Meta:
        model = Bet
        fields = ['title', 'summary', 'expiration_date', 'extendable']

    def __init__(self, *args, **kwargs):
        super(BetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3 create-label'
        self.helper.field_class = 'col-md-9'
        self.helper.layout = Layout(
            Div(
                Field('title'),
                Field('summary'),
                Field('expiration_date'),
                Field('extendable'),
                Fieldset('Add options',
                    BetOptionFormsetLayout('betoptions')),
                HTML("<br>"),
                ButtonHolder(Submit('submit', 'save')),
                )
            )


    
class BetOptionForm(forms.ModelForm):

    class Meta :
        model = BetOption
        fields = ['title']


class BetOptionFormsetLayout(LayoutObject):

    template = "bets/betoptions_formset.html"

    def __init__(self, formset_name_in_context, template=None):
        self.formset_name_in_context = formset_name_in_context
        self.fields = []
        if template:
            self.template = template

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        formset = context[self.formset_name_in_context]
        return render_to_string(self.template, {'formset': formset})
        

BetOptionFormset =  inlineformset_factory(
        Bet, BetOption, fields=('title', ), form=BetOptionForm,
        extra=0, can_delete=True, min_num=2
        )
