from django.db.models.signals import post_save
from app.models import *
from django.dispatch import receiver

@receiver(post_save, sender=BankOperation)
def update_account_after_new_operation(sender, **kwargs):
    """ Update account balance when a new operation is added """
    if kwargs['created'] :
        bank = kwargs['instance'].account
        bank.balance += kwargs['instance'].amount
        if bank.balance < 0 : bank.balance = 0
        bank.save()


@receiver(post_save, sender=Betting)
def create_operation_for_betting(sender, **kwargs):
    """ Create a new operation corresponding to a created betting """
    if kwargs['created'] :
        betting = kwargs['instance']
        user = betting.creator
        amount = -betting.amount
        title = "Betting option n°"+ str(betting.option.id) +" for the bet n°" + str(betting.option.bet.id)
        account = user.bankaccount
        ope = BankOperation(title = title, creator = user, account = account, amount = amount)
        ope.save()

